package com.starcineplex.woozy.cineplex.Parser;

import com.starcineplex.woozy.cineplex.Models.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Md. Abdur Rahim on 24-Oct-15.
 */
public class MovieParser {
    JSONArray response;

    public MovieParser(JSONArray response) {
        this.response = response;
    }

    public List<Movie> getData() {
        List<Movie> movieList = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            try {
                Movie movieJSON = getSingleMovieObject(response.getJSONObject(i));
                movieList.add(movieJSON);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return movieList;
    }


    public Movie getSingleMovieObject(JSONObject movieDetails) {
        Movie movie = new Movie();
        try {
            movie.title = movieDetails.getString(ParseConstants.TITLE);
            movie.year = movieDetails.getString(ParseConstants.YEAR);
            movie.released = movieDetails.getString(ParseConstants.RELEASED);
            movie.runtime = movieDetails.getString(ParseConstants.RUNTIME);
            movie.genre = movieDetails.getString(ParseConstants.GENRE);
            movie.director = movieDetails.getString(ParseConstants.DIRECTOR);
            movie.writer = movieDetails.getString(ParseConstants.WRITER);
            movie.actors = movieDetails.getString(ParseConstants.ACTORS);
            movie.plot = movieDetails.getString(ParseConstants.PLOT);
            movie.movieLanguage = movieDetails.getString(ParseConstants.MOVIE_LANGUAGE);
            movie.country = movieDetails.getString(ParseConstants.COUNTRY);
            movie.poster = movieDetails.getString(ParseConstants.POSTER);
            movie.imdbRating = movieDetails.getString(ParseConstants.IMDB_RATING);
            movie.imdbVotes = movieDetails.getString(ParseConstants.IMDB_VOTES);
            movie.imdbID = movieDetails.getString(ParseConstants.IMDB_ID);
            movie.showtype = movieDetails.getString(ParseConstants.SHOW_TYPE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return movie;
    }
}
