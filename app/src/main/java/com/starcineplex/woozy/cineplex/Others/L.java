package com.starcineplex.woozy.cineplex.Others;

import android.util.Log;

/**
 * Created by Md. Abdur Rahim on 24-Oct-15.
 */
public class L {
    public static void log(String log) {
        Log.d("MyApp", log);
    }

    public static void error(String log) {
        Log.e("Cineplex", log);
    }
}