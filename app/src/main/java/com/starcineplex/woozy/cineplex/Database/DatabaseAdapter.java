package com.starcineplex.woozy.cineplex.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.starcineplex.woozy.cineplex.Models.Movie;
import com.starcineplex.woozy.cineplex.Models.MovieListModel;
import com.starcineplex.woozy.cineplex.Models.MoviesTime;
import com.starcineplex.woozy.cineplex.Models.ShowTime;
import com.starcineplex.woozy.cineplex.Models.Time;
import com.starcineplex.woozy.cineplex.Models.Venue;
import com.starcineplex.woozy.cineplex.Others.L;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Md. Abdur Rahim on 24-Oct-15.
 */
public class DatabaseAdapter {
    Context context;
    DbHelper helper;
    SQLiteDatabase db;

    public DatabaseAdapter(Context context) {
        this.context = context;
        helper = new DbHelper(context);
    }

    private void openConnection() {
        db = helper.getWritableDatabase();
    }


    private void closeConnection() {
        if (db.isOpen()) {
            db.close();
        }
    }

    /**
     * Insert into NowShowing and ComingSoon Tables...
     */
    public void insertIntoMovie(List<Movie> movieList, String tableName) {
        L.log(movieList.size() + " Datareceived for " + tableName);
        if (movieList.size() > 0) {
            openConnection();
            if (tableName == DataBaseConstants.NOW_SWOWING_MOVIE_TABLE) {
                db.execSQL(DataBaseConstants.DROP_NOW_SHOWING);
                db.execSQL(DataBaseConstants.CREATE_NOW_SHOWING_TABLE);
            } else if (tableName == DataBaseConstants.COMING_SOON_MOVIE_TABLE) {
                db.execSQL(DataBaseConstants.DROP_COMING_SOON);
                db.execSQL(DataBaseConstants.CREATE_COMING_SOON_TABLE);
            }

            for (int i = 0; i < movieList.size(); i++) {
                Movie currentMovie = movieList.get(i);
                ContentValues values = new ContentValues();
                values.put(DataBaseConstants.TITLE, currentMovie.title);
                values.put(DataBaseConstants.YEAR, currentMovie.year);
                values.put(DataBaseConstants.RELEASED, currentMovie.released);
                values.put(DataBaseConstants.RUNTIME, currentMovie.runtime);
                values.put(DataBaseConstants.GENRE, currentMovie.genre);
                values.put(DataBaseConstants.DIRECTOR, currentMovie.director);
                values.put(DataBaseConstants.WRITER, currentMovie.writer);
                values.put(DataBaseConstants.ACTORS, currentMovie.actors);
                values.put(DataBaseConstants.PLOT, currentMovie.plot);
                values.put(DataBaseConstants.LANGUAGE, currentMovie.movieLanguage);
                values.put(DataBaseConstants.COUNTRY, currentMovie.country);
                values.put(DataBaseConstants.POSTER, currentMovie.poster);
                values.put(DataBaseConstants.IMDB_RATING, currentMovie.imdbRating);
                values.put(DataBaseConstants.IMDB_VOTES, currentMovie.imdbVotes);
                values.put(DataBaseConstants.IMDB_ID, currentMovie.imdbID);
                values.put(DataBaseConstants.SHOW_TYPE, currentMovie.showtype);
                try {
                    long res = db.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                    if (res != -1) {
                        L.log("Inserted in " + tableName + ": " + currentMovie.title /*+ " - Poster: " + b.toString()*/);
                    } else {
                        L.error("Failed" + currentMovie.title);
                    }
                } catch (Exception e) {
                    L.log(e.toString());
                }
            }
        }
        closeConnection();
    }


    /**
     * Insert into Showtime
     */
    public void insertIntoShowtime(List<ShowTime> showTimeList, String tableName) {
        openConnection();
        if (showTimeList.size() > 0) {
            db.execSQL(DataBaseConstants.DROP_TIME_TABLE);
            db.execSQL(DataBaseConstants.CREATE_TIME_TABLE);

            for (int i = 0; i < showTimeList.size(); i++) {
                ShowTime showTime = showTimeList.get(i);
                String dateString = showTime.Date;
                List<Venue> venueList = showTime.Venue;
                for (int j = 0; j < venueList.size(); j++) {
                    Venue venue = venueList.get(j);
                    String venueString = venue.Hall;
                    List<MoviesTime> moviesTimeList = venue.MoviesTime;
                    for (int k = 0; k < moviesTimeList.size(); k++) {
                        MoviesTime moviesTime = moviesTimeList.get(k);
                        String title = moviesTime.Title;
                        List<Time> timeList = moviesTime.Times;
                        for (int l = 0; l < timeList.size(); l++) {
                            Time time = timeList.get(l);
                            String timeString = time.Time;
                            // ContentValues...
                            ContentValues values = new ContentValues();
                            values.put(DataBaseConstants.TITLE, title);
                            values.put(DataBaseConstants.DATE, dateString);
                            values.put(DataBaseConstants.HALL, venueString);
                            values.put(DataBaseConstants.TIME, timeString);

                            try {
                                long res = db.insert(tableName, null, values);
                                if (res > 0) {
                                    //L.log("Inserted into " + tableName + ": " + dateString + " " + venueString + " " + title + " " + timeString);
                                } else {
                                    L.log("Failed" + dateString + " " + venueString + " " + title + " " + timeString);
                                }
                            } catch (Exception e) {
                                L.log(e.toString());
                            }

                        }
                    }
                }
            }
        }
        closeConnection();
    }

    /*
     * Get all movie item list
     */
    public List<MovieListModel> getMovieList(String tableName) {
        List<MovieListModel> movieList = new ArrayList<>();
        openConnection();

        String[] collumns = {DataBaseConstants.ID, DataBaseConstants.TITLE, DataBaseConstants.YEAR, DataBaseConstants.IMDB_RATING,
                DataBaseConstants.POSTER, DataBaseConstants.ACTORS, DataBaseConstants.GENRE, DataBaseConstants.SHOW_TYPE};
        Cursor cursor = db.query(tableName, collumns, null, null, null, null, null);

        if (cursor != null && cursor.moveToFirst() && cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                MovieListModel singleMovie = new MovieListModel();
                singleMovie.id = cursor.getInt(cursor.getColumnIndex(DataBaseConstants.ID));
                singleMovie.title = cursor.getString(cursor.getColumnIndex(DataBaseConstants.TITLE));
                singleMovie.year = cursor.getString(cursor.getColumnIndex(DataBaseConstants.YEAR));
                singleMovie.imdbRating = cursor.getString(cursor.getColumnIndex(DataBaseConstants.IMDB_RATING));
                singleMovie.actors = cursor.getString(cursor.getColumnIndex(DataBaseConstants.ACTORS));
                singleMovie.poster = cursor.getString(cursor.getColumnIndex(DataBaseConstants.POSTER));
                singleMovie.showtype = cursor.getString(cursor.getColumnIndex(DataBaseConstants.SHOW_TYPE));
                singleMovie.genre = cursor.getString(cursor.getColumnIndex(DataBaseConstants.GENRE));
                singleMovie.tableName = tableName;
                movieList.add(singleMovie);
            }
            ;
        }
        cursor.close();
        closeConnection();
        return movieList;
    }

    private List<Time> getTimeList(String title) {
        List<Time> timeList = new ArrayList<>();
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, MMMM d, y");
        String todayDate = simpleDateFormat.format(date).toString();
        openConnection();

        String[] column = {DataBaseConstants.TIME};
        String whereargs = DataBaseConstants.TITLE + "=? AND " + DataBaseConstants.DATE + "=?";
        String[] args = {title.toString(), todayDate};

        Cursor cursor = db.query(DataBaseConstants.TIME_TABLE, column, whereargs, args, null, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.moveToNext()) {
                    Time time = new Time();
                    time.Time = cursor.getString(cursor.getColumnIndex(DataBaseConstants.TIME));
                    timeList.add(time);
                }
            }
        } catch (Exception ex) {
            L.error(ex.toString());
        }
        closeConnection();
        return timeList;
    }

    public Movie getSingleMovie(String tableName, int id) {
        openConnection();
        String[] column = {DataBaseConstants.TITLE, DataBaseConstants.YEAR, DataBaseConstants.RELEASED,
                DataBaseConstants.RUNTIME, DataBaseConstants.GENRE, DataBaseConstants.DIRECTOR, DataBaseConstants.WRITER,
                DataBaseConstants.ACTORS, DataBaseConstants.PLOT, DataBaseConstants.LANGUAGE, DataBaseConstants.COUNTRY,
                DataBaseConstants.POSTER, DataBaseConstants.IMDB_RATING,
                DataBaseConstants.IMDB_VOTES, DataBaseConstants.IMDB_ID, DataBaseConstants.SHOW_TYPE, DataBaseConstants.POSTER_IMAGE};
        Cursor cursor = db.query(tableName, null, DataBaseConstants.ID + "=" + id, null, null, null, null, null);
        Movie movie = new Movie();
        if (cursor != null && cursor.moveToFirst()) {
            movie.title = cursor.getString(cursor.getColumnIndex(DataBaseConstants.TITLE));
            movie.year = cursor.getString(cursor.getColumnIndex(DataBaseConstants.YEAR));
            movie.released = cursor.getString(cursor.getColumnIndex(DataBaseConstants.RELEASED));
            movie.runtime = cursor.getString(cursor.getColumnIndex(DataBaseConstants.RUNTIME));
            movie.genre = cursor.getString(cursor.getColumnIndex(DataBaseConstants.GENRE));
            movie.director = cursor.getString(cursor.getColumnIndex(DataBaseConstants.DIRECTOR));
            movie.writer = cursor.getString(cursor.getColumnIndex(DataBaseConstants.WRITER));
            movie.actors = cursor.getString(cursor.getColumnIndex(DataBaseConstants.ACTORS));
            movie.showtype = cursor.getString(cursor.getColumnIndex(DataBaseConstants.SHOW_TYPE));
            movie.plot = cursor.getString(cursor.getColumnIndex(DataBaseConstants.PLOT));
            movie.movieLanguage = cursor.getString(cursor.getColumnIndex(DataBaseConstants.LANGUAGE));
            movie.country = cursor.getString(cursor.getColumnIndex(DataBaseConstants.COUNTRY));
            movie.poster = cursor.getString(cursor.getColumnIndex(DataBaseConstants.POSTER));
            movie.imdbRating = cursor.getString(cursor.getColumnIndex(DataBaseConstants.IMDB_RATING));
            movie.imdbVotes = cursor.getString(cursor.getColumnIndex(DataBaseConstants.IMDB_VOTES));
            movie.imdbID = cursor.getString(cursor.getColumnIndex(DataBaseConstants.IMDB_ID));
        }
        closeConnection();
        return movie;
    }
}


