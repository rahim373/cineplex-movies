package com.starcineplex.woozy.cineplex.VolleySupportingClass;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.starcineplex.woozy.cineplex.HttpResponseInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class HttpConnectionClass {

    public static final String NOW_SHOWING = "now_showing";
    public static final String UP_COMING = "up_coming";
    public static final String SHOW_TIME = "show_time";

    private static HashMap<String, RequestType> map = new HashMap<>();


    static {
        map.put(UP_COMING, new RequestType("GET", "http://cineplex.davinciplanners.com/upcoming.php"));
        map.put(NOW_SHOWING, new RequestType("GET", "http://cineplex.davinciplanners.com/nowshowing.php"));
        map.put(SHOW_TIME, new RequestType("GET", "http://starcineplexapi.azurewebsites.net/api/showtime"));
    }

    public static void requestHandler(final Context activityContext, String key, final HttpResponseInterface caller) {

        String url = map.get(key).getRoute();

        StringRequest stringRequest = new StringRequest(map.get(key).getType(),
                url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {

                           if(response!=null){
                               JSONArray jsonArray = new JSONArray(response);
                               caller.actionOnResponse(jsonArray);
                           }

                        } catch (JSONException e) {
                            JSONArray array = new JSONArray();
                            JSONObject jsonExceptionObject = new JSONObject();
                            try {
                                jsonExceptionObject.put("success", false);
                                jsonExceptionObject.put("message", "Exception: " + e.toString());
                                array.put(jsonExceptionObject);
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            caller.actionOnResponse(array);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                JSONArray array = new JSONArray();
                JSONObject errorJsonObject = new JSONObject();
                String errorMessage;

                if (error instanceof TimeoutError) {
                    errorMessage = "Request timed out of Connection problem";
                } else if (error instanceof ServerError) {
                    errorMessage = "Server Error";
                } else if (error instanceof NetworkError) {
                    errorMessage = "Network Error";
                } else if (error instanceof ParseError) {
                    errorMessage = "Parsing problem";
                } else if (error instanceof NoConnectionError) {
                    errorMessage = "No Internet Connection";
                } else
                    errorMessage = "Something wrong in API";

                try {
                    errorJsonObject.put("success", false);
                    errorJsonObject.put("message", errorMessage);
                    array.put(errorJsonObject);
                } catch (JSONException e) {
                    Toast.makeText(activityContext, "Exception: " + e, Toast.LENGTH_LONG).show();
                }

                caller.actionOnResponse(array);

            }

        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000, //60 seconds is timeout
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(stringRequest);

    }
}
