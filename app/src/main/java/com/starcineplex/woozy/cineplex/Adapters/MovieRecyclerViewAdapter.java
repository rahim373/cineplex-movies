package com.starcineplex.woozy.cineplex.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.starcineplex.woozy.cineplex.Activity.DetailMovieActivity;
import com.starcineplex.woozy.cineplex.Animation.AnimationUtils;
import com.starcineplex.woozy.cineplex.Models.MovieListModel;
import com.starcineplex.woozy.cineplex.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Ashif on 10/8/2015.
 */
public class MovieRecyclerViewAdapter extends RecyclerView.Adapter<MovieRecyclerViewAdapter.MyViewHolder> {
    Context context;
    LayoutInflater inflater;
    List<MovieListModel> movieList = Collections.EMPTY_LIST;
    int currentPosition = 0;

    public MovieRecyclerViewAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void setData(List<MovieListModel> movieList) {
        this.movieList = movieList;
        notifyItemRangeChanged(0, movieList.size());
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.movie_single_item_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        if (movieList.size() > 0) {
            final MovieListModel currentMovie = movieList.get(position);

            String show = currentMovie.showtype.toString();
            String posterURL = currentMovie.poster.toString();
            float rating = 0;
            if (!currentMovie.imdbRating.equals(null)) {
                if (!currentMovie.imdbRating.toString().equals("")) {
                    rating = Float.parseFloat(currentMovie.imdbRating.toString());
                } else rating = 0;
            }
            float rate = rating;
            rating = rating / 2;
            holder.ratingBar.setRating(rating);
            holder.ratingBar.setMax(10);


            if (!currentMovie.year.toString().equals("null") && !currentMovie.year.toString().equals(""))
                holder.title.setText(currentMovie.title + " (" + currentMovie.year + ")");
            else holder.title.setText(currentMovie.title);

            holder.genre.setText(currentMovie.genre);
            holder.rating.setText(rate + "");

        /* 2D/3D Check */
            if (show.equals("3D")) {
                holder.showType.setVisibility(View.VISIBLE);
            } else {
                holder.showType.setVisibility(View.INVISIBLE);
            }

        /* Poster check */
            if (posterURL.length() < 5) {
                holder.poster.setImageResource(R.drawable.dummy_poster);
            } else {
                //loadImages(currentMovie.poster.toString(), holder);
                Picasso.with(context)
                        .load(currentMovie.poster)
                        .resize(150, 225)
                        .centerCrop()// call .centerInside() or .centerCrop() to avoid a stretched image
                        .into(holder.poster);
            }

            AnimationUtils.animate(holder, true);

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent detailIntent = new Intent(context, DetailMovieActivity.class);
                    detailIntent.putExtra("id", currentMovie.id);
                    detailIntent.putExtra("tableName", currentMovie.tableName);
                    context.startActivity(detailIntent);
                }
            });

        }
    }


    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, genre, rating;
        ImageView poster, showType;
        RatingBar ratingBar;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            genre = (TextView) itemView.findViewById(R.id.genre);
            rating = (TextView) itemView.findViewById(R.id.ratingText);
            poster = (ImageView) itemView.findViewById(R.id.poster);
            showType = (ImageView) itemView.findViewById(R.id.showType);
            ratingBar = (RatingBar) itemView.findViewById(R.id.rating);
            cardView = (CardView) itemView.findViewById(R.id.card);

        }
    }
}
