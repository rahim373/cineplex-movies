package com.starcineplex.woozy.cineplex.Fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.starcineplex.woozy.cineplex.Adapters.ShowTimeAdapter;
import com.starcineplex.woozy.cineplex.Database.DataBaseConstants;
import com.starcineplex.woozy.cineplex.HttpResponseInterface;
import com.starcineplex.woozy.cineplex.Models.ShowTime;
import com.starcineplex.woozy.cineplex.Others.L;
import com.starcineplex.woozy.cineplex.Parser.ShowTimeParser;
import com.starcineplex.woozy.cineplex.R;
import com.starcineplex.woozy.cineplex.VolleySupportingClass.HttpConnectionClass;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShowtimeFragment extends Fragment implements HttpResponseInterface, SwipeRefreshLayout.OnRefreshListener {

    Context context;
    ShowTimeAdapter adapter;
    SwipeRefreshLayout swipeRefreshLayout;
    List<ShowTime> showTimeList;


    public ShowtimeFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_showtime, container, false);
        context = getActivity();
        showTimeList = new ArrayList<>();
        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.show_time_swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        RecyclerView recyclerView = (RecyclerView) layout.findViewById(R.id.ShowTimeList);
        adapter = new ShowTimeAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        doExecute();
        return layout;
    }

    private void doExecute() {
        String FileName = DataBaseConstants.SHOWTIME_FILENAME;
        JSONArray response = null;
        try {
            FileInputStream fileInputStream = context.openFileInput(FileName);
            StringBuffer buffer = new StringBuffer();
            int read = -1;
            while ((read = fileInputStream.read()) != -1) {
                buffer.append((char) read);
            }
            response = new JSONArray(buffer.toString());
            ShowTimeParser parser = new ShowTimeParser(response);
            showTimeList = parser.getShowTimeList();
            adapter.setData(showTimeList);
            adapter.notifyDataSetChanged();
        } catch (FileNotFoundException e) {
            getShowTimeResponse();
        } catch (IOException e) {
            L.error(e.toString());
        } catch (JSONException e) {
            L.error(e.toString());
        }
    }

    private void getShowTimeResponse() {
        swipeRefreshLayout.setRefreshing(true);
        HttpConnectionClass.requestHandler(context, HttpConnectionClass.SHOW_TIME, this);

    }

    @Override
    public void actionOnResponse(JSONArray obj) {
        try {
            if (obj.getJSONObject(0).getBoolean("success") == false) {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Error! " + obj.getJSONObject(0).getString("message"))
                        .show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String textToWrite = obj.toString();
        String FileName = DataBaseConstants.SHOWTIME_FILENAME;
        FileOutputStream fos = null;
        try {
            fos = context.openFileOutput(FileName, Context.MODE_PRIVATE);
            fos.write(textToWrite.getBytes());
            doExecute();
        } catch (FileNotFoundException e) {
            L.error("File not found");
        } catch (IOException e) {
            L.error(e.toString());
        } finally {
            try {
                fos.close();
                swipeRefreshLayout.setRefreshing(false);
            } catch (IOException e) {
                L.error(e.toString());
            }
        }
    }

    @Override
    public void onRefresh() {
        getShowTimeResponse();
    }
}
