package com.starcineplex.woozy.cineplex.Database;

/**
 * Created by Abdur Rahim on 20-Oct-15.
 */
public class DataBaseConstants {
    public static final String DATABASE = "Cineplex.db";
    public static final String NOW_SWOWING_MOVIE_TABLE = "NowShowingTable";
    public static final String COMING_SOON_MOVIE_TABLE = "ComingSoonTable";
    public static final String TIME_TABLE = "TimeTable";
    public static final String SHOWTIME_FILENAME = "ShowTime.txt";
    public static final int VERSION = 7;

    public static final String ID = "_id";
    public static final String TITLE = "Title";
    public static final String YEAR = "Year";
    public static final String RELEASED = "Released";
    public static final String RUNTIME = "Runtime";
    public static final String GENRE = "Genre";
    public static final String DIRECTOR = "Director";
    public static final String WRITER = "Writer";
    public static final String ACTORS = "Actors";
    public static final String PLOT = "Plot";
    public static final String LANGUAGE = "Language";
    public static final String COUNTRY = "Country";
    public static final String POSTER = "Poster";
    public static final String IMDB_RATING = "imdbRating";
    public static final String IMDB_VOTES = "imdbVotes";
    public static final String IMDB_ID = "imdbID";
    public static final String SHOW_TYPE = "Showtype";
    public static final String POSTER_IMAGE = "PosterImage";


    public static final String DATE = "Date";
    public static final String HALL = "Hall";
    public static final String TIME = "Time";


    public static final String CREATE_NOW_SHOWING_TABLE =
            "CREATE TABLE " + NOW_SWOWING_MOVIE_TABLE + " (" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    TITLE + " VARCHAR(50) UNIQUE," +
                    YEAR + " VARCHAR(4)," +
                    RELEASED + " VARCHAR(15)," +
                    RUNTIME + " VARCHAR(10)," +
                    GENRE + " VARCHAR(50)," +
                    DIRECTOR + " VARCHAR(100)," +
                    WRITER + " VARCHAR(100)," +
                    ACTORS + " VARCHAR(100)," +
                    PLOT + " VARCHAR(500)," +
                    LANGUAGE + " VARCHAR(30)," +
                    COUNTRY + " VARCHAR(30)," +
                    POSTER + " VARCHAR(100)," +
                    IMDB_RATING + " VARCHAR(5)," +
                    IMDB_VOTES + " VARCHAR(6)," +
                    IMDB_ID + " VARCHAR(10)," +
                    SHOW_TYPE + " VARCHAR(2))";

    public static final String CREATE_COMING_SOON_TABLE =
            "CREATE TABLE " + COMING_SOON_MOVIE_TABLE + " (" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    TITLE + " VARCHAR(50) UNIQUE," +
                    YEAR + " VARCHAR(4)," +
                    RELEASED + " VARCHAR(15)," +
                    RUNTIME + " VARCHAR(10)," +
                    GENRE + " VARCHAR(50)," +
                    DIRECTOR + " VARCHAR(100)," +
                    WRITER + " VARCHAR(100)," +
                    ACTORS + " VARCHAR(100)," +
                    PLOT + " VARCHAR(500)," +
                    LANGUAGE + " VARCHAR(30)," +
                    COUNTRY + " VARCHAR(30)," +
                    POSTER + " VARCHAR(100)," +
                    IMDB_RATING + " VARCHAR(5)," +
                    IMDB_VOTES + " VARCHAR(6)," +
                    IMDB_ID + " VARCHAR(10)," +
                    SHOW_TYPE + " VARCHAR(2))";

    public static final String CREATE_TIME_TABLE =
            "CREATE TABLE " + TIME_TABLE + "(" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    TITLE + " VARCHAR(30)," +
                    TIME + " VARCHAR(10)," +
                    HALL + " VARCHAR(20)," +
                    DATE + " VARCHAR(15))";

    public static final String DROP_NOW_SHOWING = "DROP TABLE IF EXISTS " + NOW_SWOWING_MOVIE_TABLE;
    public static final String DROP_COMING_SOON = "DROP TABLE IF EXISTS " + COMING_SOON_MOVIE_TABLE;
    public static final String DROP_TIME_TABLE = "DROP TABLE IF EXISTS " + TIME_TABLE;


}
