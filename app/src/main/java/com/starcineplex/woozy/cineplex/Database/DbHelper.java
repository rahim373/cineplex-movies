package com.starcineplex.woozy.cineplex.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.starcineplex.woozy.cineplex.Others.L;

/**
 * Created by Md. Abdur Rahim on 23-Apr-16.
 */

public class DbHelper extends SQLiteOpenHelper {

    private static final int VERSION = DataBaseConstants.VERSION;
    private static final String DATABASE_NAME = DataBaseConstants.DATABASE;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(DataBaseConstants.CREATE_NOW_SHOWING_TABLE);
            db.execSQL(DataBaseConstants.CREATE_COMING_SOON_TABLE);
        } catch (Exception e) {
            L.log(e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DataBaseConstants.DROP_NOW_SHOWING);
        db.execSQL(DataBaseConstants.DROP_COMING_SOON);
        db.execSQL(DataBaseConstants.DROP_TIME_TABLE);
        onCreate(db);
    }
}
