package com.starcineplex.woozy.cineplex.VolleySupportingClass;

import com.android.volley.Request;

/**
 * Created by hasan on 4/23/16.
 */
public class RequestType {

    private String type;
    private String route;

    public RequestType(String type, String route) {
        this.type = type;
        this.route = route;
    }


    public int getType() {
        if (this.type.equalsIgnoreCase("POST"))
            return Request.Method.POST;
        else
            return Request.Method.GET;

    }

    public String getRoute() {
        return this.route;
    }
}
