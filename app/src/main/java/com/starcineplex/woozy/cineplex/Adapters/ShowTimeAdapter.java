package com.starcineplex.woozy.cineplex.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.starcineplex.woozy.cineplex.Models.MoviesTime;
import com.starcineplex.woozy.cineplex.Models.ShowTime;
import com.starcineplex.woozy.cineplex.Models.Time;
import com.starcineplex.woozy.cineplex.Models.Venue;
import com.starcineplex.woozy.cineplex.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Md. Abdur Rahim on 22-Oct-15.
 */
public class ShowTimeAdapter extends RecyclerView.Adapter<ShowTimeAdapter.MyHolder> {
    Context context;
    LayoutInflater inflater;
    private List<ShowTime> data = Collections.EMPTY_LIST;

    public ShowTimeAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.show_time_single_day, parent, false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        ShowTime showTime = data.get(position);
        holder.firstHall.removeAllViews();
        holder.date.setText(showTime.Date);

        List<Venue> venueList = showTime.Venue;

        // First Venue
        Venue venue0 = venueList.get(0);
        holder.firstHall.removeAllViews();
        List<MoviesTime> moviesTimes0 = venue0.MoviesTime;
        for (int i = 0; i < moviesTimes0.size(); i++) {
            MoviesTime moviesTime = moviesTimes0.get(i);
            View view = inflater.inflate(R.layout.show_nested_single_movie, null);
            TextView title = (TextView) view.findViewById(R.id.Title);
            title.setText(moviesTime.Title);
            holder.firstHall.addView(view);
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.TimeBox);
            List<Time> times = moviesTime.Times;
            for (int j = 0; j < times.size(); j++) {
                Time time = times.get(j);
                TextView timeTV = new TextView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 30, 0);
                timeTV.setLayoutParams(params);
                timeTV.setText(time.Time);
                timeTV.setTextColor(Color.rgb(120, 120, 120));
                linearLayout.addView(timeTV);
            }
        }

        // Second Venue
        Venue venue1 = venueList.get(1);
        holder.secondHall.removeAllViews();
        List<MoviesTime> moviesTimes1 = venue1.MoviesTime;
        for (int i = 0; i < moviesTimes1.size(); i++) {
            MoviesTime moviesTime = moviesTimes1.get(i);
            View view = inflater.inflate(R.layout.show_nested_single_movie, null);
            TextView title = (TextView) view.findViewById(R.id.Title);
            title.setText(moviesTime.Title);
            holder.secondHall.addView(view);
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.TimeBox);
            List<Time> times = moviesTime.Times;
            for (int j = 0; j < times.size(); j++) {
                Time time = times.get(j);
                TextView timeTV = new TextView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 30, 0);
                timeTV.setLayoutParams(params);
                timeTV.setText(time.Time);
                timeTV.setTextColor(Color.rgb(120, 120, 120));
                linearLayout.addView(timeTV);
            }
        }

        // Third Venue
        Venue venue2 = venueList.get(2);
        holder.thirdHall.removeAllViews();
        List<MoviesTime> moviesTimes2 = venue2.MoviesTime;
        for (int i = 0; i < moviesTimes2.size(); i++) {
            MoviesTime moviesTime = moviesTimes2.get(i);
            View view = inflater.inflate(R.layout.show_nested_single_movie, null);
            TextView title = (TextView) view.findViewById(R.id.Title);
            title.setText(moviesTime.Title);
            holder.thirdHall.addView(view);
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.TimeBox);
            List<Time> times = moviesTime.Times;
            for (int j = 0; j < times.size(); j++) {
                Time time = times.get(j);
                TextView timeTV = new TextView(context);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 30, 0);
                timeTV.setLayoutParams(params);
                timeTV.setText(time.Time);
                timeTV.setTextColor(Color.rgb(120, 120, 120));
                linearLayout.addView(timeTV);
            }
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<ShowTime> data) {
        this.data = data;
        notifyItemRangeChanged(0, data.size());
    }

    class MyHolder extends RecyclerView.ViewHolder {
        TextView date;
        LinearLayout firstHall, secondHall, thirdHall;

        public MyHolder(View itemView) {
            super(itemView);

            date = (TextView) itemView.findViewById(R.id.date);
            firstHall = (LinearLayout) itemView.findViewById(R.id.FirstHallBox);
            secondHall = (LinearLayout) itemView.findViewById(R.id.SecondHallBox);
            thirdHall = (LinearLayout) itemView.findViewById(R.id.ThirdHallBox);

        }
    }
}
