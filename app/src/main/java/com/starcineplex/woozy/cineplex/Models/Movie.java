package com.starcineplex.woozy.cineplex.Models;

/**
 * Created by Ashif on 10/8/2015.
 */
public class Movie {
    public String title;
    public String year;
    public String released;
    public String runtime;
    public String genre;
    public String director;
    public String writer;
    public String actors;
    public String showtype;
    public String plot;
    public String movieLanguage;
    public String country;
    public String poster;
    public String imdbRating;
    public String imdbVotes;
    public String imdbID;
    public byte[] posterImage;
}