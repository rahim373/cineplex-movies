package com.starcineplex.woozy.cineplex.Parser;

import android.util.Log;

import com.starcineplex.woozy.cineplex.Models.MoviesTime;
import com.starcineplex.woozy.cineplex.Models.ShowTime;
import com.starcineplex.woozy.cineplex.Models.Time;
import com.starcineplex.woozy.cineplex.Models.Venue;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Md. Abdur Rahim on 22-Oct-15.
 */

public class ShowTimeParser {
    JSONArray response;

    public ShowTimeParser(JSONArray response) {
        this.response = response;
    }

    public List<ShowTime> getShowTimeList() {
        List<ShowTime> showTimeList = new ArrayList<>();
        try {
            for (int i = 0; i < response.length(); i++) {

                JSONObject dateobject = response.getJSONObject(i);
                ShowTime showTime = new ShowTime();
                showTime.Date = dateobject.getString(ParseConstants.DATE);
                JSONArray venueResponse = dateobject.getJSONArray(ParseConstants.VENUE);

                List<Venue> venueList = new ArrayList<>();
                for (int j = 0; j < venueResponse.length(); j++) {
                    JSONObject venueObject = venueResponse.getJSONObject(j);
                    Venue venue = new Venue();
                    venue.Hall = venueObject.getString(ParseConstants.HALL);
                    JSONArray moviesResponse = venueObject.getJSONArray(ParseConstants.MOVIETIMES);

                    List<MoviesTime> moviesTimeList = new ArrayList<>();
                    for (int k = 0; k < moviesResponse.length(); k++) {
                        JSONObject movieObject = moviesResponse.getJSONObject(k);
                        MoviesTime moviesTime = new MoviesTime();
                        moviesTime.Title = movieObject.getString(ParseConstants.TITLE);
                        JSONArray timesResponse = movieObject.getJSONArray(ParseConstants.TIMES);

                        List<Time> timeList = new ArrayList<>();
                        for (int l = 0; l < timesResponse.length(); l++) {
                            JSONObject timesObject = timesResponse.getJSONObject(l);
                            Time time = new Time();
                            time.Time = timesObject.getString(ParseConstants.TIME);
                            timeList.add(time);
                        }
                        moviesTime.Times = timeList;
                        moviesTimeList.add(moviesTime);
                    }
                    venue.MoviesTime = moviesTimeList;
                    venueList.add(venue);
                }
                showTime.Venue = venueList;
                showTimeList.add(showTime);
            }
        } catch (Exception ex) {
            Log.d("Cineplex", ex.toString());
        }

        return showTimeList;
    }


}
