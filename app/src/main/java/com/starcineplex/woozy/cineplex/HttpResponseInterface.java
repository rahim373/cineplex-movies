package com.starcineplex.woozy.cineplex;

import org.json.JSONArray;

/**
 * Created by hasan on 4/23/16.
 */
public interface HttpResponseInterface {

    void actionOnResponse(JSONArray obj);
}