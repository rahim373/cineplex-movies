package com.starcineplex.woozy.cineplex.Parser;

/**
 * Created by Md. Abdur Rahim on 22-Oct-15.
 */
public class ParseConstants {
    public static final String DATE = "Date";
    public static final String VENUE = "Venue";
    public static final String HALL = "Hall";
    public static final String MOVIETIMES = "MoviesTime";
    public static final String TIMES = "Times";
    public static final String TIME = "Time";


    public static final String TITLE = "Title";
    public static final String YEAR = "Year";
    public static final String RATED = "Rated";
    public static final String RELEASED = "Released";
    public static final String RUNTIME = "Runtime";
    public static final String GENRE = "Genre";
    public static final String DIRECTOR = "Director";
    public static final String WRITER = "Writer";
    public static final String ACTORS = "Actors";
    public static final String PLOT = "Plot";
    public static final String MOVIE_LANGUAGE = "Language";
    public static final String COUNTRY = "Country";
    public static final String AWARDS = "Awards";
    public static final String POSTER = "Poster";
    public static final String METASCORE = "Metascore";
    public static final String IMDB_RATING = "imdbRating";
    public static final String IMDB_VOTES = "imdbVotes";
    public static final String IMDB_ID = "imdbID";
    public static final String TYPE = "Type";
    public static final String RESPONSE = "Response";
    public static final String SHOW_TYPE = "Showtype";
}
