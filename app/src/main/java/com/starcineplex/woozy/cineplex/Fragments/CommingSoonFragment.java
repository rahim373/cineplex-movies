package com.starcineplex.woozy.cineplex.Fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.starcineplex.woozy.cineplex.Adapters.MovieRecyclerViewAdapter;
import com.starcineplex.woozy.cineplex.Database.DataBaseConstants;
import com.starcineplex.woozy.cineplex.Database.DatabaseAdapter;
import com.starcineplex.woozy.cineplex.HttpResponseInterface;
import com.starcineplex.woozy.cineplex.Models.Movie;
import com.starcineplex.woozy.cineplex.Models.MovieListModel;
import com.starcineplex.woozy.cineplex.Parser.MovieParser;
import com.starcineplex.woozy.cineplex.R;
import com.starcineplex.woozy.cineplex.VolleySupportingClass.HttpConnectionClass;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CommingSoonFragment extends Fragment implements HttpResponseInterface, SwipeRefreshLayout.OnRefreshListener {
    private MovieRecyclerViewAdapter adapter;
    private ProgressBar loading;
    private RecyclerView recyclerView;
    private LinearLayout errors;
    private SwipeRefreshLayout swipeRefreshLayout;
    private DatabaseAdapter dbAdapter;
    private List<MovieListModel> movieListModallist;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_comming_soon, container, false);
        dbAdapter = new DatabaseAdapter(getActivity());
        movieListModallist = new ArrayList<>();
        recyclerView = (RecyclerView) layout.findViewById(R.id.commingSoonmovieRecyclerView);
        errors = (LinearLayout) layout.findViewById(R.id.errors);
        loading = (ProgressBar) layout.findViewById(R.id.loading);
        swipeRefreshLayout = (SwipeRefreshLayout) layout.findViewById(R.id.coming_soon_swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        adapter = new MovieRecyclerViewAdapter(getActivity());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });


        doExecute();
        return layout;
    }

    private void doExecute() {
        movieListModallist = dbAdapter.getMovieList(DataBaseConstants.COMING_SOON_MOVIE_TABLE);
        if (movieListModallist.size() > 0) {
            adapter.setData(movieListModallist);
            adapter.notifyDataSetChanged();
        } else {
            getComingSoonResponse();
        }
    }

    @Override
    public void actionOnResponse(JSONArray obj) {
        MovieParser parser = new MovieParser(obj);
        List<Movie> parsedData = parser.getData();
        dbAdapter.insertIntoMovie(parsedData, DataBaseConstants.COMING_SOON_MOVIE_TABLE);
        movieListModallist = dbAdapter.getMovieList(DataBaseConstants.COMING_SOON_MOVIE_TABLE);
        adapter.setData(movieListModallist);
        adapter.notifyDataSetChanged();
        if (movieListModallist.size() == 0) {
            try {
                new AlertDialog.Builder(getActivity())
                        .setMessage("Error! " + obj.getJSONObject(0).getString("message"))
                        .show();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        getComingSoonResponse();
    }

    public void getComingSoonResponse() {
        swipeRefreshLayout.setRefreshing(true);
        HttpConnectionClass.requestHandler(getActivity(), HttpConnectionClass.UP_COMING, this);
    }
}