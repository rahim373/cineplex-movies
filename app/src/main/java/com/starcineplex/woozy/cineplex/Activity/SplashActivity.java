package com.starcineplex.woozy.cineplex.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.starcineplex.woozy.cineplex.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        Thread display = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                    Intent changeActivity = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(changeActivity);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        };
        display.start();
    }

}
