package com.starcineplex.woozy.cineplex.Animation;

import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Md. Abdur Rahim on 11-Oct-15.
 */
public class AnimationUtils {
    public static void animate(RecyclerView.ViewHolder holder, boolean goesDown) {
        ObjectAnimator animateTransY = ObjectAnimator.ofFloat(holder.itemView, "translationY", goesDown == true ? 50 : -50, 0);
        animateTransY.setDuration(1000);
        animateTransY.start();
    }
}
