package com.starcineplex.woozy.cineplex.Fragments;


import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.starcineplex.woozy.cineplex.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class LocationmapFragment extends Fragment {


    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 0;
    WebView webView;
    TextView tel, email;

    public LocationmapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_locationmap, container, false);
        tel = (TextView) layout.findViewById(R.id.tel);
        email = (TextView) layout.findViewById(R.id.email);

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                emailIntent.setType("vnd.android.cursor.item/email");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info@cineplexbd.com"});
                startActivity(Intent.createChooser(emailIntent, "Send mail using..."));
            }
        });

        tel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL_PHONE);
                }else{
                    makeCall();
                }
            }
        });

        webView = (WebView) layout.findViewById(R.id.webView);
        String url = "https://www.google.com.bd/maps/place/Star+Cineplex,+Show+Motion+Limited+,+Level+8,13%2F3+Ka+Panthapath,+Dhaka+1205/@23.7512047,90.390079,18z/data=!4m2!3m1!1s0x3755b8a3303a6fbf:0xbc442f814508a7f0?hl=bn";
        webView.setWebViewClient(new MyBrowser());
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(url);

        return layout;
    }

    private void makeCall() {
        String telephone = "tel:+88029138260";
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(telephone));
        startActivity(intent);
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    makeCall();

                } else {

                    new AlertDialog.Builder(getActivity())
                            .setMessage("Error! No permission to make call")
                            .show();
                }
                return;
            }
        }
    }


}
