package com.starcineplex.woozy.cineplex.Activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.starcineplex.woozy.cineplex.Fragments.AboutFragment;
import com.starcineplex.woozy.cineplex.Fragments.CommingSoonFragment;
import com.starcineplex.woozy.cineplex.Fragments.LocationmapFragment;
import com.starcineplex.woozy.cineplex.Fragments.NowShowingFragment;
import com.starcineplex.woozy.cineplex.Fragments.ShowtimeFragment;
import com.starcineplex.woozy.cineplex.Fragments.TicketsFragment;
import com.starcineplex.woozy.cineplex.R;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    boolean doubleBackToExitPressedOnce = false;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setToolbar();
        setNavigationDrawerAndTitle(savedInstanceState);
        loadAdMob(); // Google admob
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setNavigationDrawerAndTitle(Bundle savedInstanceState) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.now_showing);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().replace(R.id.mainFrame, new NowShowingFragment()).commit();
        }
        setTitle("Now Showing");
    }

    private void loadAdMob() {
        /*final AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("A06443D66DBE4D108201FAF47E946D55")
                .build();
        mAdView.loadAd(adRequest);*/

        final AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdFailedToLoad(int errorCode) {
                super.onAdFailedToLoad(errorCode);
                ViewGroup.LayoutParams params = mAdView.getLayoutParams();
                params.height = 0;
                mAdView.setLayoutParams(params);
            }
        });
    }

    @Override
    public void onBackPressed() {
        // Handle Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // Override double click
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                finish();
                System.exit(0);
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.exitString, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 1000);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        String title = getResources().getString(R.string.app_name);

        if (id == R.id.now_showing) {
            fragment = new NowShowingFragment();
            title = getResources().getString(R.string.now_showing);
        } else if (id == R.id.coming_soon) {
            fragment = new CommingSoonFragment();
            title = getResources().getString(R.string.coming_soon);
        } else if (id == R.id.showtime) {
            fragment = new ShowtimeFragment();
            title = getResources().getString(R.string.show_time);
        } else if (id == R.id.location_map) {
            fragment = new LocationmapFragment();
            title = getResources().getString(R.string.location_map);
        } else if (id == R.id.ticket) {
            fragment = new TicketsFragment();
            title = getResources().getString(R.string.ticket);
        } else if (id == R.id.about) {
            fragment = new AboutFragment();
            title = getResources().getString(R.string.action_about);
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.mainFrame, fragment);
            setTitle(title);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
