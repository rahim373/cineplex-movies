package com.starcineplex.woozy.cineplex.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.starcineplex.woozy.cineplex.Database.DatabaseAdapter;
import com.starcineplex.woozy.cineplex.Models.Movie;
import com.starcineplex.woozy.cineplex.Others.L;
import com.starcineplex.woozy.cineplex.R;

public class DetailMovieActivity extends AppCompatActivity {
    Toolbar toolbar;
    String tableName = "";
    int id;
    TextView title, actors, ratingText, genre, director, writer, plot, runtime, released, rated, language, country, awared, metaScore, imdbRating, imdbVotes;
    ImageView poster, showTypeImg;
    RatingBar rating;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        addToolBar();
        initialize();


        Intent intent = getIntent();
        if (intent.hasExtra("id") && intent.hasExtra("tableName")) {
            tableName = intent.getStringExtra("tableName");
            id = intent.getIntExtra("id", 0);

            UpdateUI updateUI = new UpdateUI();
            updateUI.execute();

        }
    }

    private void addToolBar() {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initialize() {
        context = this;
        title = (TextView) findViewById(R.id.titleText);
        actors = (TextView) findViewById(R.id.actorsText);
        ratingText = (TextView) findViewById(R.id.ratingText);
        genre = (TextView) findViewById(R.id.genreText);
        director = (TextView) findViewById(R.id.directorText);
        writer = (TextView) findViewById(R.id.writerText);
        runtime = (TextView) findViewById(R.id.runtimeText);
        released = (TextView) findViewById(R.id.releasedText);
        language = (TextView) findViewById(R.id.languageText);
        country = (TextView) findViewById(R.id.countryText);
        imdbRating = (TextView) findViewById(R.id.imdmRatingText);
        imdbVotes = (TextView) findViewById(R.id.imdbVotesText);
        plot = (TextView) findViewById(R.id.plotText);
        showTypeImg = (ImageView) findViewById(R.id.showTypeImg);
        poster = (ImageView) findViewById(R.id.posterImg);
        rating = (RatingBar) findViewById(R.id.rating);
    }


    // Handler class
    class UpdateUI extends AsyncTask {
        Movie movie = new Movie();

        @Override
        protected Object doInBackground(Object[] params) {

            DatabaseAdapter dbAdapter = new DatabaseAdapter(context);
            movie = dbAdapter.getSingleMovie(tableName, id);
            L.log(movie.title);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            String cast = new String(movie.actors);
            String show = movie.showtype.toString();
            String posterURL = movie.poster.toString();

            float rate = 0;
            if (!movie.imdbRating.equals(null)) {
                if (!movie.imdbRating.toString().equals("")) {
                    rate = Float.parseFloat(movie.imdbRating.toString());
                } else rate = 0;
            }
            float r = rate;
            rate = rate / 2;
            rating.setRating(rate);
            rating.setMax(10);

            if (!movie.year.toString().equals("null") && !movie.year.toString().equals(""))
                title.setText(movie.title + " (" + movie.year + ")");
            else title.setText(movie.title);
            actors.setText(cast.replace(", ", "\n"));
            ratingText.setText(movie.imdbRating);
            genre.setText(movie.genre);
            director.setText(movie.director);
            writer.setText(movie.writer);
            runtime.setText(movie.runtime);
            released.setText(movie.released);
            language.setText(movie.movieLanguage);
            country.setText(movie.country);
            imdbRating.setText(r + "");
            imdbVotes.setText(movie.imdbVotes);
            plot.setText(movie.plot);
            getSupportActionBar().setSubtitle(movie.title);


            /* 2D/3D Check */
            if (show.equals("3D")) {
                showTypeImg.setVisibility(showTypeImg.VISIBLE);
            } else {
                showTypeImg.setVisibility(showTypeImg.INVISIBLE);
            }


            /* Poster check */
            if (posterURL.length() < 5) {
                poster.setImageResource(R.drawable.dummy_poster);
            } else {
                Picasso.with(context)
                        .load(movie.poster)
                        .resize(150, 225)
                        .centerCrop()// call .centerInside() or .centerCrop() to avoid a stretched image
                        .into(poster);
            }
        }
    }
}


