package com.starcineplex.woozy.cineplex.Models;

import java.util.List;

/**
 * Created by Md. Abdur Rahim on 26-Oct-15.
 */
public class MovieListModel {
    public int id;
    public String title;
    public String year;
    public String genre;
    public String actors;
    public List<Time> timeList;
    public String showtype;
    public String poster;
    public String imdbRating;
    public String tableName;
}
